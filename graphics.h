#ifndef GRAPHICS_H
#define GRAPHICS_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <SOIL/SOIL.h>
#include <graphene.h>

#define WINDOW_WIDTH 800
#define WINDOW_HEIGHT 600
#define WINDOW_TITLE "OpenGL test"
#define VERTEX_SHADER_FILE "vertex.glsl"
#define FRAGMENT_SHADER_FILE "fragment.glsl"

GLFWwindow *init(void);
void cleanUp(void);
void compileShader(GLchar*, GLenum, GLuint);
void loadTexture(GLchar*, int, GLchar*, GLuint, GLuint[]);

#endif
