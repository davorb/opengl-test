#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <SOIL/SOIL.h>
#include <graphene.h>

#include "graphics.h"
#include "camera.h"

#define VERTEX_SHADER_FILE "vertex.glsl"
#define FRAGMENT_SHADER_FILE "fragment.glsl"
#define BRICK_IMAGE_FILE "cat.jpg"
#define DOTS_IMAGE_FILE "dots.jpg"

static void keyCallback(GLFWwindow *window, int key, int scancode, int action, int mods) {
  (void)scancode;
  (void)mods;

  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
    glfwSetWindowShouldClose(window, GL_TRUE);
  }

  if (action == GLFW_PRESS || action == GLFW_REPEAT) {
    if (key == GLFW_KEY_W) {
      cameraMoveForward();
    } else if (key == GLFW_KEY_S) {
      cameraMoveBack();
    }
  }

  if (key == GLFW_KEY_A) {
    cameraMoveLeft();
  }
  if (key == GLFW_KEY_D) {
    cameraMoveRight();
  }
}

int main() {
  GLFWwindow *window = init();
  GLuint vao;
  glGenVertexArrays(1, &vao);
  glBindVertexArray(vao);

  GLuint vbo, ebo;
  glGenBuffers(1, &vbo);
  glGenBuffers(1, &ebo);

  GLfloat vertices[] = {
    -0.5f,  0.5f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
    0.5f,  0.5f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f,
    0.5f, -0.5f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,
    -0.5f, -0.5f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f
  };
  GLuint elements[] = {
    0, 1, 2,
    2, 3, 0
  };

  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER,
               sizeof(vertices),
               vertices, GL_STATIC_DRAW);

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER,
               sizeof(elements),
               elements, GL_STATIC_DRAW);

  GLuint shader_program = glCreateProgram();

  compileShader(VERTEX_SHADER_FILE, GL_VERTEX_SHADER, shader_program);
  compileShader(FRAGMENT_SHADER_FILE, GL_FRAGMENT_SHADER, shader_program);

  glBindFragDataLocation(shader_program, 0, "outColor");

  glLinkProgram(shader_program);
  glUseProgram(shader_program);

  GLint position_attrib = glGetAttribLocation(shader_program, "position");
  glEnableVertexAttribArray(position_attrib);
  glVertexAttribPointer(position_attrib, 2, GL_FLOAT, GL_FALSE,
                        7*sizeof(GLfloat), 0);

  GLint color_attrib = glGetAttribLocation(shader_program, "color");
  glEnableVertexAttribArray(color_attrib);
  glVertexAttribPointer(color_attrib, 3, GL_FLOAT, GL_FALSE,
                        7*sizeof(GLfloat), (void*)(2*sizeof(GLfloat)));

  GLint texture_attrib = glGetAttribLocation(shader_program, "texcoord");
  glEnableVertexAttribArray(texture_attrib);
  glVertexAttribPointer(texture_attrib, 2, GL_FLOAT, GL_FALSE,
                        7*sizeof(GLfloat), (void*)(5*sizeof(GLfloat)));

  GLint uni_trans = glGetUniformLocation(shader_program, "model");
  graphene_matrix_t *model_trans = graphene_matrix_alloc();
  float flat_trans[16];
  graphene_matrix_init_identity(model_trans);

  printf("Loading textures.\n");
  GLuint textures[2];
  glGenTextures(2, textures);

  glActiveTexture(GL_TEXTURE0);
  loadTexture(DOTS_IMAGE_FILE, 0, "texDots", shader_program, textures);
  glActiveTexture(GL_TEXTURE1);
  loadTexture(BRICK_IMAGE_FILE, 1, "texBrick", shader_program, textures);

  initCamera(shader_program);

  //glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
  //glfwSetCursorPos(window, 0, 0);
  glfwSetCursorPosCallback(window, cameraMouseMoveCallback);

  glfwSetKeyCallback(window, keyCallback);

  GLenum GLerror;
  if ((GLerror = glGetError()) == GL_NO_ERROR) {
    fprintf(stderr, "GL error at initialization: %x\n", GLerror);
  }

  double current_time = glfwGetTime();
  double time_diff = 0;
  printf("Starting main loop.\n");
  while (!glfwWindowShouldClose(window)) {
    glfwPollEvents();
    time_diff = glfwGetTime() - current_time;
    current_time = glfwGetTime();

    //graphene_matrix_rotate_z(model_trans, (float)(20*time_diff));
    //graphene_matrix_rotate_x(model_trans, (float)(20*time_diff));
    graphene_matrix_to_float(model_trans, flat_trans);
    glUniformMatrix4fv(uni_trans, 1, GL_FALSE, flat_trans);

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

    glfwSwapBuffers(window);

    if ((GLerror = glGetError()) != GL_NO_ERROR) {
      fprintf(stderr, "GL error during running: %x\n", GLerror);
      exit(1);
    }
  }

  glDeleteProgram(shader_program);
  // TODO: add glDeleteShader()
  glDeleteBuffers(1, &ebo);
  glDeleteBuffers(1, &vbo);
  glDeleteVertexArrays(1, &vao);
  cleanUp();
  exit(EXIT_SUCCESS);
}
