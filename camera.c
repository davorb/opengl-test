#include "camera.h"

double oldXpos, oldYpos;
graphene_matrix_t *view_matrix;
graphene_vec3_t *eye, *center;
GLint uni_view;

void initCamera(GLuint shader_program) {
  graphene_matrix_t *projection;
  float flat_view[16], flat_proj[16];
  view_matrix = graphene_matrix_alloc();
  projection = graphene_matrix_alloc();
  eye = graphene_vec3_alloc();
  center = graphene_vec3_alloc();

  graphene_vec3_init(eye, 0.0f, 0.2f, 0.8f);
  graphene_vec3_init(center, 0.0f, 0.0f, 0.0f);
  graphene_matrix_init_look_at(view_matrix, eye, center, graphene_vec3_y_axis());

  graphene_matrix_init_perspective(projection, 90.0f, ASPECT, 0.0f, 1.0f);

  graphene_matrix_to_float(view_matrix, flat_view);
  graphene_matrix_to_float(projection, flat_proj);
  uni_view = glGetUniformLocation(shader_program, "view");
  GLint uni_proj = glGetUniformLocation(shader_program, "proj");
  glUniformMatrix4fv(uni_view, 1, GL_FALSE, flat_view);
  glUniformMatrix4fv(uni_proj, 1, GL_FALSE, flat_proj);

  graphene_matrix_free(projection);
}

void freeCamera() {
  graphene_matrix_free(view_matrix);
}

void cameraMouseMoveCallback(GLFWwindow *window, double xpos, double ypos) {
  (void)window;
  double deltaXpos, deltaYpos;
  deltaXpos = xpos - oldXpos;
  deltaYpos = ypos - oldYpos;

  oldXpos = xpos;
  oldYpos = ypos;
}

void cameraMoveForward() {
  graphene_vec3_t *deltaX = graphene_vec3_alloc();
  graphene_vec3_init(deltaX, 0.0f, 0.0f, MOVEMENT_SPEED);
  cameraMove(deltaX);
  graphene_vec3_free(deltaX);
}

void cameraMoveBack() {
  graphene_vec3_t *deltaX = graphene_vec3_alloc();
  graphene_vec3_init(deltaX, 0.0f, 0.0f, -MOVEMENT_SPEED);
  cameraMove(deltaX);
  graphene_vec3_free(deltaX);
}

void cameraMoveLeft() {
  graphene_vec3_t *deltaX = graphene_vec3_alloc();
  graphene_vec3_init(deltaX, MOVEMENT_SPEED, 0.0f, 0.0f);
  cameraMove(deltaX);
  graphene_vec3_free(deltaX);
}

void cameraMoveRight() {
  graphene_vec3_t *deltaX = graphene_vec3_alloc();
  graphene_vec3_init(deltaX, -MOVEMENT_SPEED, 0.0f, 0.0f);
  cameraMove(deltaX);
  graphene_vec3_free(deltaX);
}


static void cameraMove(graphene_vec3_t *delta) {
  graphene_vec3_subtract(center, delta, center);
  graphene_vec3_subtract(eye, delta, eye);
  graphene_matrix_init_look_at(view_matrix, eye, center, graphene_vec3_y_axis());

  float flat_view[16];
  graphene_matrix_to_float(view_matrix, flat_view);
  glUniformMatrix4fv(uni_view, 1, GL_FALSE, flat_view);
}
