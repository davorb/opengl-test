#include "graphics.h"

void loadTexture(GLchar* file, int n, GLchar* name,
                 GLuint shader_program,
                 GLuint textures[]) {
  glBindTexture(GL_TEXTURE_2D, textures[n]);
  int width, height;
  unsigned char* image =
    SOIL_load_image(file, &width, &height, 0, SOIL_LOAD_RGB);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0,
               GL_RGB, GL_UNSIGNED_BYTE, image);
  SOIL_free_image_data(image);
  glUniform1i(glGetUniformLocation(shader_program, name), n);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
}

void compileShader(GLchar *file, GLenum shader_type,
                   GLuint shader_program) {
  printf("Compiling shader %s\n", file);
  char *buffer;
  FILE *fp = fopen(file, "r");
  if (fp == NULL) {
    fprintf(stderr, "Unable to read file: %s\n", file);
    exit(1);
  }
  fseek(fp, 0L, SEEK_END);
  size_t size = (size_t) ftell(fp);
  rewind(fp);
  buffer = malloc(size);
  if (buffer == NULL) {
    fprintf(stderr, "malloc failed.\n");
    exit(1);
  }

  fread(buffer, size, 1, fp);
  buffer[size] = '\0';

  GLuint shader = glCreateShader(shader_type);
  const GLchar *shader_source = (GLchar*) buffer;
  glShaderSource(shader, 1, &shader_source, NULL);
  glCompileShader(shader);

  GLint compilation_status;
  glGetShaderiv(shader, GL_COMPILE_STATUS, &compilation_status);
  if (compilation_status != GL_TRUE) {
    char error_buffer[512];
    glGetShaderInfoLog(shader, 512, NULL, error_buffer);
    fprintf(stderr, "%s\n", error_buffer);
    exit(1);
  }
  glAttachShader(shader_program, shader);
  fclose(fp);
  free(buffer);
}

GLFWwindow *init() {
  glfwInit();
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);

  GLFWwindow *window = glfwCreateWindow(WINDOW_WIDTH,
                                        WINDOW_HEIGHT,
                                        WINDOW_TITLE,
                                        NULL,
                                        NULL);
  glfwMakeContextCurrent(window);
  glewExperimental = GL_TRUE;
  if (glewInit() != GLEW_OK) {
    fprintf(stderr, "glewInit failed.\n");
    exit(1);
  }
  return window;
}

void cleanUp() {
  glfwTerminate();
}
