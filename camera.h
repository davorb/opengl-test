#ifndef CAMERA_H
#define CAMERA_H

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <SOIL/SOIL.h>
#include <graphene.h>

#define ASPECT 800.0f/600.0f
#define MOUSE_SENSITIVITY 1.0f
#define MOVEMENT_SPEED 0.14f

extern double oldXpos, oldYpos;

extern graphene_matrix_t *view_matrix;
extern graphene_vec3_t *eye, *center;
extern GLint uni_view;

void initCamera(GLuint);
void freeCamera(void);

void cameraMouseMoveCallback(GLFWwindow*, double, double);
void cameraMoveForward(void);
void cameraMoveBack(void);
void cameraMoveForward(void);
void cameraMoveBack(void);

static void cameraMove(graphene_vec3_t*);

#endif
