CC=clang
CFLAGS=-Weverything -fsanitize=undefined -std=c11 -g
GLFW=`pkg-config --cflags glfw3` `pkg-config --libs glfw3`
GLEW=`pkg-config --cflags glew` `pkg-config --libs glew`
SOIL=-lSOIL
GRAPHENE=-isystem/home/davor/bin/include/graphene-1.0 -isystem/home/davor/bin/lib/graphene-1.0/include -L/home/davor/bin/lib/ -lgraphene-1.0

opengl-test: main.c graphics.c camera.c
	$(CC) -o opengl-test *.c $(CFLAGS) $(GLFW) $(GLEW) $(SOIL) $(GRAPHENE)

clean:
	rm -f opengl-test
