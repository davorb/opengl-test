#version 150

in vec3 Color;
in vec2 Texcoord;

out vec4 outColor;

uniform sampler2D texBrick;
uniform sampler2D texDots;

void main() {
  vec4 colBrick = texture(texBrick, Texcoord);
  vec4 colDots = texture(texDots, Texcoord);
  outColor = mix(colBrick, colDots, 0.5);
}
